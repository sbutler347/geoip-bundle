<?php

namespace DL\GeoIPBundle\Service;

require_once __DIR__ . '/Net_GeoIP.php';

class GeoIP extends Net_GeoIP
{
    public function __construct()
    {
        parent::__construct(__DIR__.'/../Resources/geoip/geoip-data.dat');
    }

    /**
     * @param string $ipaddress
     * @param string $countryCode
     *
     * @return boolean
     */
    public function isCountryCode($ipaddress, $countryCode)
    {
        return $countryCode === $this->lookupCountryCode($ipaddress);
    }

    /**
     * @param string $ipaddress
     *
     * @return boolean
     */
    public function getCountryCode($ipaddress)
    {
        return $this->lookupCountryCode($ipaddress);
    }
}
